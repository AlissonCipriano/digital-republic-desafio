# Digital Republic Desafio 

Desafio técnico para a Digital Republic.


- [Objetivo](#objetivo)
- [O que deve ser desenvolvido](#requisitos)
- [Você pode acessar o projeto por esse link](#acesso)
- [Como rodar](#instrucoes)


<a name="objetivo"/>

## Objetivo
O Objetivo desse desafio é avaliar o conhecimento e capacidade dos candidatos às vagas de programação/desenvolvimento.
O teste pode ser feito por qualquer nível de profissional, contudo o critério de avaliação será conforme a experiencia do candidato.


<a name="requisitos"/>

## O que deve ser desenvolvido
Uma aplicação web ou mobile que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede.
Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L


<a name="acesso"/>

## Você pode acessar o projeto por esse link
[App de Cálculo de Tinta](https://develop--heartfelt-conkies-bab9d9.netlify.app/ "App de Cálculo de Tinta").


<a name="instrucoes"/>

## Como rodar
Se preferir, rode o app através do seu computador, seguindo os passos a seguir:

- Faça o download do projeto executando o comando git clone git@gitlab.com:AlissonCipriano/digital-republic-desafio.git no seu terminal (necessário ter o Git instalado no sistema)
- Entre na pasta do projeto
- Abra um terminal dentro da pasta do projeto e faça o download das dependências través do comando npm install (necessário ter o Node e o NPM instalados no sistema)
- Após a finalização do download, inicie o projeto executando o comando npm run dev
- Um servidor local será criado e o link de acesso será apresentado na tela (o início deve ser 192 ou localhost)
- Copie o link de acesso e abra em um navegador

### ou, se preferir, faça o build do projeto
- Execute normalmente todas as etapas a seguir, mas, em vez de rodar o comando npm run dev, execute npm run build
- O projeto será compilado com todas as dependências e arquivos necessários, assim que esse procesos finalizar, abra a pasta dist
- Dentro dela, haverá um arquivo index.html, abra-o em um navegador
