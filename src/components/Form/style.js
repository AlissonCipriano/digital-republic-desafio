import styled from "styled-components";

export const StyledForm = styled.div`
    padding: 15px;
    max-width: 800px;

    button.Add,
    button.Calc {
        padding: 15px;
        border: 0;
        background-color: var(--blue5);
        color: #fff;
        border-radius: 4px;
        font-family: Poppins;
        text-transform: uppercase;
        font-weight: 800;
        cursor: pointer;
        margin-right: 15px;

        &:disabled {
            opacity: .5;
            cursor: inherit;
        }
    }

    &.blocked .WallContainer {
        pointer-events: none;
        opacity: .5;
    }
`;

export const FormHeader = styled.div`
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: center;

    h1 {
        margin-top: 30px;
        color: var(--grey4);
    }

    p {
        width: 65%;
        margin-top: 8px;
        color: var(--grey5);
    }
`;

export const FormContainer = styled.form`
    margin-top: 25px;
    box-sizing: border-box;
    padding: 15px;

    #mensagem, #resposta {
        text-align: center;
        padding: 15px;
        margin-bottom: 25px;
        border-radius: 4px;
        background-color: #ff6e6e;
        color: #fff;
    }
    #resposta {
        background-color: #6e73ff;
    }

    .WallContainer {
        margin-bottom: 20px;

        span.title {
            font-weight: 400;
            display: block;
            margin-bottom: 8px;
        }

        .WallContainer-input-group {
            box-sizing: border-box;
            padding: 15px;
            border: 1px solid var(--grey3);
            border-radius: 4px;
            display: flex;
            flex-wrap: wrap;
            align-items: center;

            .WallContainer-input-container {
                display: flex;
                align-items: center;
                margin-right: 25px;
                margin-bottom: 15px;
                margin-right: auto;

                label {
                    display: flex;
                    align-items: center;
                    height: 35px;
                    padding: 0 10px;
                    border-radius: 4px 0 0 4px;
                    border: 1px solid var(--grey3);
                    background-color: var(--blue4);
                    color: #fff;
                }

                input {
                    position: relative;
                    height: 35px;
                    border-radius: 0 4px 4px 0;
                    border: 1px solid var(--grey3);
                    box-sizing: border-box;
                    padding: 0 15px;
                    outline: none;
                    margin-left: -1px;
                    width: auto;

                    &.error {
                        background-color: #ffd7d7;
                        border-color: #ffb5b5;
                    }
                }

                span.info {
                    display: block;
                    margin-left: 5px;
                    font-size: .9em;
                    padding: 5px;
                    border-radius: 4px;
                    background-color: var(--grey3);
                }
            }
        }
    }
`;
