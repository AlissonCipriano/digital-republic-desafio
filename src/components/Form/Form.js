import React, { useState, useEffect, useContext } from 'react';
import { StyledForm, FormHeader, FormContainer } from './style';

// Contexts
import Comodo from '../../contexts/Comodo';

const Form = ({allowAdd}) => {
    const [mensagem, setMensagem] = useState(null);
    const [valores, setValores] = useState(null);
    const [totalParede, setTotalParede] = useState(null);
    const { comodo, setComodo } = useContext(Comodo);
    const [paredeAtual, setParedeAtual] = useState(1);
    const latasTinta = [18, 3.6, 2.5, 0.5];
    const [resposta, setResposta] = useState(null);


    const handleAdd = {
        clean: () => {
            setValores(null);
            setTotalParede(null);
        },
        getValues: (evt) => {
            evt.preventDefault();
            handleAdd.clean();

            setValores({
                medidas: {
                    altura: {valor: document.getElementById('altura').value, id: 'altura'},
                    largura: {valor: document.getElementById('largura').value, id: 'largura'}
                },
                portasJanelas: {
                    portas: {valor: document.getElementById('portas').value, id: 'portas'},
                    janelas: {valor: document.getElementById('janelas').value, id: 'janelas'}
                }
            });
        },
        checkEmpty: () => {
            const arrayInputs = [
                valores.medidas.altura,
                valores.medidas.largura,
                valores.portasJanelas.portas,
                valores.portasJanelas.janelas
            ];

            let errorCounter = 0;

            arrayInputs.map((input) => {
                if (!input.valor) {
                    errorCounter++;
    
                    document.getElementById(input.id).classList.add('error');
                }
            });
    
            if (errorCounter > 0) {
                setMensagem("Digite um valor positivo para todos os campos.");
            }
            else {
                handleAdd.checkTotalArea();
            }
        },
        checkTotalArea: () => {
            const totalParede = (Math.round(parseFloat(valores.medidas.altura.valor) * 100) / 100) * (Math.round(parseFloat(valores.medidas.largura.valor) * 100) / 100);

            if ((totalParede < 1 || totalParede > 15)) {
                setMensagem("A área da parede deve ser de no mínimo 1 metro quadrado e no máximo 15 metros quadrados.");
            }
            else {
                setTotalParede(totalParede);
            }
        },
        comparePortasJanelas: () => {
            const totalJanelas = (2.40 * valores.portasJanelas.janelas.valor);
            const totalPortas = (1.52 * valores.portasJanelas.portas.valor);

            console.log("Total da área das portas + as janelas: ", totalJanelas + totalPortas);
            console.log("Valor de 50% da area da parede: ", (50 / 100) * totalParede);

            if ((totalJanelas + totalPortas) > (50 / 100) * totalParede) {
                setMensagem("O total de área das portas e janelas deve ser no máximo 50% da área de parede.");   
            }
            else {
                handleAdd.checkMinWallHeight();
            }
        },
        checkMinWallHeight: () => {
            if (valores.portasJanelas.portas.valor > 0) {
                console.clear();
                console.log("Altura da parede: ", valores.portasJanelas.portas.valor);
                console.log("Altura da parede - altura da porta: ", valores.medidas.altura.valor - 1.52);
                console.log("Altura da porta: ", 1.52);

                if ((valores.medidas.altura.valor - 0.30) < 1.52) {
                    setMensagem("A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta.");
                }
                else {
                    handleAdd.addWall();
                }
            }
            else {
                handleAdd.addWall();
            }
        },
        addWall: () => {
            const totalParedes = comodo.paredes.lenght;

            setComodo({paredes: [...comodo.paredes, valores]});

            const arrayInputs = [
                valores.medidas.altura,
                valores.medidas.largura,
                valores.portasJanelas.portas,
                valores.portasJanelas.janelas
            ];

            arrayInputs.map((input) => {
                document.getElementById(input.id).value = "";
            });

            

            setParedeAtual(paredeAtual + 1);

            handleAdd.clean();
        }
    }

    const handleCalc = {
        checkWalls: (evt) => {
            evt.preventDefault();

            if(comodo.paredes.length === 4) {
                handleCalc.calcTint();
            }
            else {
                setMensagem("Digite um valor positivo para todos os campos.");
                setComodo({paredes: []});
                handleAdd.clean();
            }
        },
        calcTint: () => {
            let areaTotal =  (Math.round(parseFloat(comodo.paredes[0].medidas.altura.valor) * 100) / 100) * (Math.round(parseFloat(comodo.paredes[0].medidas.largura.valor) * 100) / 100);
                areaTotal += (Math.round(parseFloat(comodo.paredes[1].medidas.altura.valor) * 100) / 100) * (Math.round(parseFloat(comodo.paredes[1].medidas.largura.valor) * 100) / 100);
                areaTotal += (Math.round(parseFloat(comodo.paredes[2].medidas.altura.valor) * 100) / 100) * (Math.round(parseFloat(comodo.paredes[2].medidas.largura.valor) * 100) / 100);
                areaTotal += (Math.round(parseFloat(comodo.paredes[3].medidas.altura.valor) * 100) / 100) * (Math.round(parseFloat(comodo.paredes[3].medidas.largura.valor) * 100) / 100);

            let totalPortas =  parseFloat(comodo.paredes[0].portasJanelas.portas.valor * 1.52);
                totalPortas += parseFloat(comodo.paredes[1].portasJanelas.portas.valor * 1.52);
                totalPortas += parseFloat(comodo.paredes[2].portasJanelas.portas.valor * 1.52);
                totalPortas += parseFloat(comodo.paredes[3].portasJanelas.portas.valor * 1.52);

            let totalJanelas =  parseFloat(comodo.paredes[0].portasJanelas.janelas.valor * 2.4);
                totalJanelas += parseFloat(comodo.paredes[1].portasJanelas.janelas.valor * 2.4);
                totalJanelas += parseFloat(comodo.paredes[2].portasJanelas.janelas.valor * 2.4);
                totalJanelas += parseFloat(comodo.paredes[3].portasJanelas.janelas.valor * 2.4);

            console.clear();
            console.log("Área total do cômodo: " + areaTotal + "m²");
            console.log("A área total do cômodo menos o valor da área das portas e janelas é:", areaTotal - (totalPortas + totalJanelas));

            let litrosNecessarios = (areaTotal - (totalPortas + totalJanelas)) / 5;

            console.log("A quantia necessária de tinta para pintar o cômodo é de: ", litrosNecessarios);

            let _resposta = '';
            let contLata = 0;

            // Checar se preciso de mais de 18 litros
            latasTinta.map((lataTinta) => {
                if (lataTinta > 0.5) {
                    if (litrosNecessarios >= lataTinta) {
                        while (litrosNecessarios >= lataTinta) {
                            litrosNecessarios = litrosNecessarios - lataTinta;
                            contLata++;
                        }
        
                        if (litrosNecessarios > 0) {
                            if (contLata > 1){
                                _resposta += contLata + ' latas de ' + lataTinta + 'L + ';
                            }
                            else {
                                _resposta += contLata + ' lata de ' + lataTinta + 'L + ';
                            }
                        }
                        else {
                            if (contLata > 1){
                                _resposta += contLata + ' latas de ' + lataTinta + 'L ';
                            }
                            else {
                                _resposta += contLata + ' lata de ' + lataTinta + 'L ';
                            }
                        }
                    }
                }
                else {
                    if (litrosNecessarios > 0) {
                        contLata = 0;
        
                        while (litrosNecessarios > 0) {
                            litrosNecessarios = litrosNecessarios - 0.5;
                            contLata++;
                        }
        
                        if (litrosNecessarios > 0) {
                            if (contLata > 1){
                                _resposta += contLata + ' latas de 0.5L + ';
                            }
                            else {
                                _resposta += contLata + ' lata de 0.5L + ';
                            }
                        }
                        else {
                            if (contLata > 1){
                                _resposta += contLata + ' latas de 0.5L ';
                            }
                            else {
                                _resposta += contLata + ' lata de 0.5L ';
                            }
                        }
                    }
                }
            });
            
            console.log(_resposta);

            setResposta(`Você deve comprar ${_resposta}`);
        }
    }

    useEffect(() => {
        if (valores) {
            handleAdd.checkEmpty();
        }
    }, [valores]);

    useEffect(() => {
        if (totalParede) {
            handleAdd.comparePortasJanelas();
        }
    }, [totalParede]);

    useEffect(() => {
        if (mensagem) {
            setTimeout(() => {
                setMensagem(null);

                var errors = document.querySelectorAll(".error");

                errors.forEach(error => {
                    error.classList.remove('error');
                });
            }, 3000);
        }
    }, [mensagem]);

    return (
        <StyledForm className={`Form ${paredeAtual === 5 ? 'blocked' : ''}`}>
            <FormHeader>
                <h1>Bem vindo!</h1>
                <p>Aplicativo para fazer o cálculo da quantidade de tinta necessária para pintar um cômodo.</p>
            </FormHeader>

            <FormContainer>
                {
                    mensagem &&
                    <p id='mensagem'>{ mensagem }</p>
                }

                {
                    resposta &&
                    <p id='resposta'>{ resposta }</p>
                }

                <div className='WallContainer'>
                    <span className='title'>Parede { paredeAtual === 5 ? '' : paredeAtual }</span>

                    <div className='WallContainer-input-group'>
                        <div className='WallContainer-input-container'>
                            <label>Altura</label>
                            <input type="number" id='altura' />
                            <span className='info'>m</span>
                        </div>

                        <div className='WallContainer-input-container'>
                            <label>Largura</label>
                            <input type="number" id='largura' />
                            <span className='info'>m</span>
                        </div>

                        <div className='WallContainer-input-container'>
                            <label>Nº de portas</label>
                            <input type="number" id='portas' />
                        </div>

                        <div className='WallContainer-input-container'>
                            <label>Nº de janelas</label>
                            <input type="number" id='janelas' />
                        </div>
                    </div>
                </div>

                <button
                    className='Add'
                    id='Add'
                    onClick={(evt) => handleAdd.getValues(evt)}
                    disabled={!allowAdd}
                >
                    Adicionar parede
                </button>

                {
                    !allowAdd &&
                    <button
                        className='Calc'
                        onClick={(evt) => handleCalc.checkWalls(evt)}
                    >
                        Calcular
                    </button>
                }
            </FormContainer>
        </StyledForm>
    );
}
 
export default Form;