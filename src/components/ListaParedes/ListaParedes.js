import React, { useState, useEffect, useContext } from 'react';
import { StyledListaParedes } from './style';

// Contexts
import Comodo from '../../contexts/Comodo';

const ListaParedes = () => {
    const { comodo, setComodo } = useContext(Comodo);
    
    if (comodo.paredes.length > 0)
        return (
            <StyledListaParedes>
                {
                    comodo.paredes.map((parede, index) => (
                        <li key={index}>
                            <div className='parede-container'>
                                <span>Altura: </span>
                                {parede.medidas.altura.valor}
                                <i>metros</i>
                            </div>

                            <div className='parede-container'>
                                <span>Largura: </span>
                                {parede.medidas.largura.valor}
                                <i>metros</i>
                            </div>

                            <div className='parede-container'>
                                <span>Número de portas: </span>
                                {parede.portasJanelas.portas.valor}
                            </div>

                            <div className='parede-container'>
                                <span>Número de janelas: </span>
                                {parede.portasJanelas.janelas.valor}
                            </div>
                        </li>
                    ))
                }
            </StyledListaParedes>
        );
    return <></>
}
 
export default ListaParedes;