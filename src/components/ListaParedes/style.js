import styled from "styled-components";

export const StyledListaParedes = styled.ul`
    list-style: none;
    margin: 0;
    padding: 0;

    li {
        box-sizing: border-box;
        padding: 15px;
        border: 1px solid var(--grey3);
        border-radius: 4px;
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: space-around;
        font-family: 'Poppins';
        margin-top: 10px;
    }

    .parede-container {
        font-weight: 400;
        display: flex;

        span {
            font-weight: 800;
            display: block;
            margin-right: 5px;
        }

        i {
            display: block;
            margin-left: 5px;
            font-style: inherit;
        }
    }

    @media (max-width: 768px) {
        li {
            justify-content: flex-start;
        }
        .parede-container {
            margin-right: 15px;
        }
    }
`;