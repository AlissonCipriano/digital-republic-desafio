import React, { useState, useEffect } from 'react';
import Form from './components/Form/Form';
import GlobalStyle from "./Styles/global";

// Contexts
import Comodo from "./contexts/Comodo";
import ListaParedes from './components/ListaParedes/ListaParedes';

const App = () => {
    const [comodo, setComodo] = useState({paredes: []});
    const [allowAdd, setAllowAdd] = useState(true);

    useEffect(() => {
        if (comodo.paredes.length === 4) {
            setAllowAdd(!allowAdd);
        }
        console.log("Paredes: ", comodo.paredes.length);
    }, [comodo]);

    return (
        <div className='App'>
            <GlobalStyle />
            <Comodo.Provider value={{comodo, setComodo}}>
                <Form allowAdd={allowAdd} />
                <ListaParedes />
            </Comodo.Provider>
        </div>
    );
}
 
export default App;