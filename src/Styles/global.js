import { createGlobalStyle } from "styled-components";


export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        text-decoration: none;
    }
    :root {
        --purple1: #642381;
        --purple2: #2D235F;
        --blue1: #0390D5;
        --blue2: #234C8E;
        --blue3: #1D6CB5;
        --blue4: #048FD1;
        --blue5: #0F6BAA;
        --grey1: #F4F4F4;
        --grey2: #F0EBEB;
        --grey3: #dee2e6;
        --grey4: #282828;
        --grey5: #5D5D5D;
    }
    html, body {
        margin: 0;
        padding: 0;
    }
    html, body, h1, p, li, span {
        font-family: 'Poppins', sans-serif;
    }
    #main {
        width: 100%;
        height: 100vh;
        overflow-y: auto;
    }
    #main,
    #app {
        width: 100%;
        display: flex;
        font-family: 'Poppins', sans-serif;
    }
    #app {
        flex-direction: column;
        align-items: center;
        justify-content: flex-start;
        padding-top: 50px;
    }

`;